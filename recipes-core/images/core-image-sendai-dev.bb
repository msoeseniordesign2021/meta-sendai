SUMMARY = "Development Sendai image"

require core-image-sendai-base.inc

IMAGE_FEATURES += "ssh-server-dropbear debug-tweaks"

IMAGE_INSTALL += "gdbserver alsa-utils"
