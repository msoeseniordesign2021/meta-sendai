inherit juce

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "gitsm://git@gitlab.com/msoeseniordesign2021/sendaigranularsynthesiser.git;protocol=ssh;branch=master"

PV = "1.0+git${SRCPV}"
SRCREV = "8cf3ba6fa69e4e76f7d3a9ac522cdbc96ae1d37e"

S = "${WORKDIR}/git"
B = "${S}/Builds"


JUCE_JUCERS = "${S}/SendaiGranularSynthesiser.jucer"
FILES:${PN} += "/DefaultConfig"

do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	cd ${B}/SendaiHardware
	oe_runmake CONFIG=Release
}

do_install () {
	install -D ${B}/SendaiHardware/build/SendaiGranularSynthesiser ${D}${bindir}/SendaiGranularSynthesiser	
	cp -R --no-dereference --preserve=mode,links -v ${S}/DefaultConfig ${D}
}

